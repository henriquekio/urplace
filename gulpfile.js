var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('default', ['sass', 'watch']);

gulp.task('sass', function(){
    return gulp.src('public/css/sass/*.sass')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('watch', function () {
    gulp.watch('public/css/sass/*.sass', ['sass']);
});