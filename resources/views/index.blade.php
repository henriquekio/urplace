<!doctype html>
<html lang="pt-bt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>UrPlace</title>
    <link rel="icon" href="{{asset('img/favicon.png')}}">
    {!! Html::style('bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::style('js/vendor/owlCarousel/dist/assets/owl.carousel.min.css') !!}
    {!! Html::style('js/vendor/owlCarousel/dist/assets/owl.theme.default.min.css') !!}
    {!! Html::script('js/vendor/jquery.min.js') !!}
    {!! Html::script('js/vendor/scrollme/jquery.scrollme.js') !!}
    {!! Html::script('js/vendor/alton/jquery.alton.min.js') !!}
</head>
<body>
<div class="wrapper">
    <section class="header">
        <div class="home">
            <div class="topbar">
                <div class="container">
                    <ul class="list-inline top-icons visible-md visible-lg text-right">
                        <li>
                            <a href="http://facebook.com/urplaceapp"
                               target="_blank">{!! Html::image('img/facebook.png', 'facebook', ['class' => 'img-responsive']) !!}</a>
                        </li>
                        <li>
                            <a href="http://instagram.com/urplaceapp" target="_blank">
                                {!! Html::image('img/instagram.png', 'instagram', ['class' => 'img-responsive']) !!}
                            </a>
                        </li>
                    </ul>
                    <ul class="list-inline visible-sm visible-xs text-center">
                        <li>
                            <a href="http://facebook.com/urplaceapp">{!! Html::image('img/facebook.png', 'facebook', ['class' => 'img-responsive']) !!}</a>
                        </li>
                        <li>
                            <a href="http://instagram.com/urplaceapp" target="_blank">
                                {!! Html::image('img/instagram.png', 'instagram', ['class' => 'img-responsive']) !!}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="logo center-block">
                <div class="img-topo center-block visible-md visible-lg">
                    {!! Html::image('img/home.png', 'topo', ['class' => 'center-block', 'style' => 'padding-top: 80px']) !!}
                </div>
                {!! Html::image('img/logo.png', 'logo', ['class' => 'img-responsive logo-img center-block']) !!}
                <h1 class="text-center">O seu lugar ideal.</h1>
                <div class="form-email">
                    <h3 class="text-center subtitulo">Se inscreva para saber quando for lançado.</h3>
                    <div class="input-group">
                        <input placeholder="Endereço de Email" class="form-control" type="email" id="email" required>
                        <span class="input-group-btn">
                        <button class="btn" type="button" id="save-email">
                            {!! Html::image('img/arrow-right.png', 'Cadastrar Email') !!}
                        </button>
                    </span>
                    </div>
                </div>
                <div class="visible-md visible-xs phone-responsive">
                    {!! Html::image('img/home.png', 'topo', ['class' => 'center-block img-responsive']) !!}
                </div>
            </div>
            <div class="container-fluid visible-md visible-lg icon-mouse">
                <div class="mouse center-block">
                    <div class="scroll"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="pageWrapper">
        {{--INICIO SOBRE--}}
        <div class="container item-app-container">
            <div class="row">
                <div class="col-md-7 visible-lg visible-md">
                    {!! Html::image('img/img-1.png', 'app', ['class' => 'img-responsive']) !!}
                </div>
                <div class="col-md-5 col-xs-12">
                    <h1 class="titulo">SOBRE O APP</h1>
                    <div class="descricao text-justify">
                        <p>Gerêncie suas preferências e escolha o local adequado para seu entretenimento. Muitas pessoas
                            sabem o que querem, mas não sabem quais ambientes oferecem o que elas procuram. O UrPlace
                            filtra esses gostos para gerar lugares baseados nessas escolhas.</p>
                    </div>
                    {!! Html::image('img/google-play.png', 'Google Play', ['class' => 'gplay img-responsive visible-md visible-lg']) !!}
                    {!! Html::image('img/google-play.png', 'Google Play', ['class' => 'gplay img-responsive visible-xs visible-sm center-block']) !!}
                </div>
            </div>
        </div>
        {{--FIM SOBRE--}}
        {{--INICIO TELA INICIAL--}}
        <div class="container item-app-container">
            <div class="row">
                <div class="col-md-5 col-xs-12">
                    <h1 class="titulo-descricao">INICIO</h1>
                    <div class="descricao text-justify">
                        <p>UrPlace funciona como um catálogo de ambientes. Seu foco principal é mostrar para você locais
                            adequados aos seus gostos. Filtre, escolha e se divirta.</p>
                    </div>
                </div>
                <div class="col-md-7 col-xs-12">
                    {!! Html::image('img/inicio-1.png', 'inicio', ['class' => 'img-responsive visible-xs visible-sm center-block']) !!}
                    <div class="img-container-right visible-lg visible-md">
                        <div class="item-1 animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="100">
                            {!! Html::image('img/inicio-1.png', 'inicio', ['class' => 'img-responsive pull-right']) !!}
                        </div>
                        <div class="item-2  animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="-100">
                            {!! Html::image('img/inicio-2.png', 'inicio', ['class' => 'img-responsive']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--FIM TELA INICIAL--}}
        {{--INICIO FILTRO--}}
        <div class="container item-app-container">
            <div class="row">
                <div class="col-md-7 visible-lg visible-md">
                    <div class="img-container-left">
                        <div class="item-1 animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="100">
                            {!! Html::image('img/filtro-1.png', 'inicio', ['class' => 'img-responsive']) !!}
                        </div>
                        <div class="item-2 animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="-100">
                            {!! Html::image('img/filtro-2.png', 'inicio', ['class' => 'img-responsive']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <h1 class="titulo">FILTRO</h1>
                    <div class="descricao text-justify">
                        <p>O filtro do UrPlace veio para revolucionar os filtros comuns. Descubra novos locais com base
                            na busca realizada. Escolha seu tipo de música, comida, bebida, ambiente e tenha um
                            entretenimento adequado ao seu gosto.</p>
                        {!! Html::image('img/filtro-1.png', 'inicio', ['class' => 'img-responsive center-block visible-sm visible-xs']) !!}
                    </div>
                </div>
            </div>
        </div>
        {{--FIM FILTRO--}}
        {{--INICIO MAPA--}}
        <div class="container item-app-container">
            <div class="row">
                <div class="col-md-5">
                    <h1 class="titulo">MAPA</h1>
                    <div class="descricao text-justify">
                        <p>Independente de onde você estiver, ative o GPS e ache locais diversos com base em sua
                            geolocalização. Navegue livremente pelo mapa buscando ambientes desejados.</p>
                    </div>
                </div>
                <div class="col-md-7 col-xs-12">
                    {!! Html::image('img/mapa-1.png', 'inicio', ['class' => 'img-responsive center-block visible-xs visible-sm']) !!}
                    <div class="img-container-right visible-lg visible-md">
                        <div class="item-1 animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="100">
                            {!! Html::image('img/mapa-1.png', 'inicio', ['class' => 'img-responsive pull-right']) !!}
                        </div>
                        <div class="item-2 animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                             data-translatey="-100">
                            {!! Html::image('img/mapa-2.png', 'inicio', ['class' => 'img-responsive']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--FIM MAPA--}}
        {{--INICIO TELAS--}}
        <div class="container-fluid telas-container">
            <div class="row">
                <div class="col-md-12 telas">
                    <h2 class="text-center titulo">TELAS</h2>
                    <div class="container margin-top-30 visible-lg visible-md">
                        <div class="owl-carousel telas-slider">
                            <div>
                                {!! Html::image('img/slider/1.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/2.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/3.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/4.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/5.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/6.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/7.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/8.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/9.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/10.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/11.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/12.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/13.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/14.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/15.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/16.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/17.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/18.png', 'telas app') !!}
                            </div>
                            <div>
                                {!! Html::image('img/slider/19.png', 'telas app') !!}
                            </div>
                        </div>
                    </div>
                    {!! Html::image('img/telas-responsive.png', 'logo', ['class' => 'img-responsive visible-xs visible-sm margin-top-20 center-block']) !!}
                </div>
            </div>
        </div>
        {{--FIM TELAS--}}
        {{--INICIO COMENTARIOS--}}
        <div class="container comentarios">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center titulo text-uppercase">O Seu Lugar Ideal</h2>
                    <div class="descricao">
                        <p class="text-center">Muitas pessoas sabem o que querem, mas não sabem quais ambientes
                            oferecem o que elas procuram, e o UrPlace está aqui para mudar isso. Nosso objetivo é
                            auxiliar o usuário a encontrar o melhor lugar que ele poderia desejar.</p>
                    </div>
                </div>
            </div>
        </div>
        {{--FIM COMENTARIOS--}}
        {{--INICIO DESENVOLVEDORES--}}
        <div class="container">
            <h2 class="text-center text-uppercase titulo">Desenvolvedores</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="container-desenvolvedores">
                        <div>
                            {!! Html::image('/img/desenvolvedores/1.png', 'desenvolvedor', ['class' => 'img-responsive center-block img-dev']) !!}
                        </div>
                        <div class="sociais">
                            <div class="sociais-list">
                                <ul class="list-inline text-center">
                                    <li>
                                        <a href="http://facebook.com/raiquebezerra" target="_blank">
                                            {!! Html::image('img/facebook-white.png', 'facebook', ['class' => 'img-responsive']) !!}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://behance.net/raiquebezerra" target="_blank">
                                            {!! Html::image('img/behance-white.png', 'facebook', ['class' => 'img-responsive']) !!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="info-desenvolvedores text-center">
                        <h2>Raíque Bezerra<br>
                            <small>UI/UX Designer</small>
                        </h2>
                        <ul class="list-inline text-center visible-xs visible-sm">
                            <li>
                                <a href="http://facebook.com/raiquebezerra" target="_blank">
                                    {!! Html::image('img/facebook-red.png', 'facebook', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                            <li>
                                <a href="http://behance.net/raiquebezerra" target="_blank">
                                    {!! Html::image('img/behance-red.png', 'facebook', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="container-desenvolvedores">
                        <div>
                            {!! Html::image('/img/desenvolvedores/2.png', 'desenvolvedor', ['class' => 'img-responsive center-block img-dev']) !!}
                        </div>
                        <div class="sociais">
                            <div class="sociais-list">
                                <ul class="list-inline text-center">
                                    <li>
                                        <a href="http://facebook.com/thiago.leitex" target="_blank">
                                            {!! Html::image('img/facebook-white.png', 'facebook', ['class' => 'img-responsive']) !!}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://behance.net/thiagoleitep" target="_blank">
                                            {!! Html::image('img/behance-white.png', 'facebook', ['class' => 'img-responsive']) !!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="info-desenvolvedores text-center">
                        <h2>Thiago Leite<br>
                            <small>Designer/Tester</small>
                        </h2>
                        <ul class="list-inline text-center visible-xs visible-sm">
                            <li>
                                <a href="http://facebook.com/thiago.leitex" target="_blank">
                                    {!! Html::image('img/facebook-red.png', 'facebook', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                            <li>
                                <a href="http://behance.net/thiagoleitep" target="_blank">
                                    {!! Html::image('img/behance-red.png', 'facebook', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid margin-top-40 margin-bottom-20">
            <button class="center-block back-top animateme scrollme" data-when="span" data-from="0.75" data-to="0"
                    data-opacity="0" data-translatey="200" id="top"
                    type="button">{!! Html::image('img/arrow-up.png') !!}</button>
        </div>
        {{--FIM DESENVOLVEDORES--}}
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-1 col-xs-6">
                        {!! Html::image('img/logo.png', 'logo', ['class' => 'img-responsive center-block']) !!}
                    </div>
                    <div class="col-md-11 visible-lg visible-md">
                        <ul class="list-inline socials-footer text-right">
                            <li>
                                <a href="http://facebook.com/urplaceapp" target="_blank">
                                    {!! Html::image('img/facebook.png', 'facebook', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                            <li>
                                <a href="http://instagram.com/urplaceapp" target="_blank">
                                    {!! Html::image('img/instagram.png', 'instagram', ['class' => 'img-responsive']) !!}
                                </a>
                            </li>
                            <li>
                                {!! Html::image('img/android.png', 'android', ['class' => 'img-responsive']) !!}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </section>
</div>
{!! Html::script('bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('js/vendor/owlCarousel/dist/owl.carousel.min.js') !!}
{!! Html::script('js/vendor/sweetalert/dist/sweetalert.min.js') !!}
{!! Html::script('js/main.js') !!}
</body>
</html>