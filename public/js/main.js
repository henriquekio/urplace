$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * @description inicialização do hero scroll
 */
$(document).alton({
    firstClass: 'header',
    bodyContainer: 'pageWrapper',
    scrollMode: 'headerScroll'
});

$(document).ready(function () {
    /**
     * @description inicialização do slider da seção telas
     */
    $(".telas-slider").owlCarousel({
        center: true,
        items: 5,
        loop: true,
        startPosition: 3,
        nav: true,
        navText: ''
    });

    /**
     * @description inicialização do slider comentários
     */
    $(".slider-comentarios").owlCarousel({
        center: true,
        items: 3,
        loop: true,
        nav: true,
        margin: 50,
        navText: ''
    });

    /**
     *@description evento de click para inicializar a função back to top no botão do footer
     */
    $("#top").click(function () {
        $("html,body").animate(
            {scrollTop: 0},
            700);
    });

    $("#save-email").click(function () {
        var email = $('#email').val();
        if (email.length > 5) {
            cadastraEmail(email);

            return false;
        }
        swal("Por favor, insira um endereço de email válido", "", "info");
    });

    $("#email").keypress(function (e) {
        if (e.keyCode === 13) {
            var email = $('#email').val();
            if (email.length > 5) {
                cadastraEmail(email);

                return false;
            }
            swal("Por favor, insira um endereço de email válido", "", "info");
        }
    });
});

function cadastraEmail(email) {
    var request = $.ajax({
        type: "POST",
        url: "/cadastra-email",
        data: {email: email},
        dataType: "JSON"
    });

    request.done(function (data) {
        if (data === false) {
            swal("Seu email já está cadastrado em nosso banco de dados.", "Em breve novidades.", "info");
        } else {
            swal("Seu email foi cadastrado com sucesso.", "Obrigado.", "success");
        }
    });

    request.fail(function (data) {
        if (data.responseJSON.email) {
            swal("Por favor, insira um endereço de email válido", "", "info");

            return false;
        }
        swal("Não foi possível inserir o seu email", "Tente novamente mais tarde.", "error");
    });
}
