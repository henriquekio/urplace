<?php

namespace App\Http\Controllers;

use App\Repositories\UsuarioRepositoryEloquent;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /** @var  UsuarioRepositoryEloquent */
    protected $usuarioRepository;

    /**
     * UsuarioController constructor.
     * @param UsuarioRepositoryEloquent $usuarioRepository
     */
    public function __construct(UsuarioRepositoryEloquent $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }


    public function cadastraEmail(Request $request)
    {
        $this->validate($request, [
           'email' => ['required', 'max:255', 'email']
        ]);

        $email = $request->email;
        if ($this->verificaEmailCadastrado($email)) {
            $result = $this->usuarioRepository->updateOrCreate($request->all());
            return response()->json($result);
        }

        return response()->json(false);
    }

    public function verificaEmailCadastrado($email)
    {
        $result = $this->usuarioRepository->findWhere(['email' => $email]);
        $result = count($result) > 0 ? false : true;

        return $result;
    }
}
